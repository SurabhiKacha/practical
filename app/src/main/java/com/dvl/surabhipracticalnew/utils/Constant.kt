package com.dvl.surabhipracticalnew.utils

object Constant {

    const val APP_NAME = "PracticalApp"
    const val NEWS_DETAILS = "NEWS_DETAILS"

    interface API_RESPONSE_STATUS {
        companion object {
            const val SUCCESS = "ok"
        }
    }

}