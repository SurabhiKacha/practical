package com.dvl.surabhipracticalnew.di

import com.dvl.surabhipracticalnew.repository.NewsListRepository
import com.dvl.surabhipracticalnew.retrofit.ApiInterface
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun newsListRepository(
        apiInterface: ApiInterface
    ): NewsListRepository {
        return NewsListRepository(apiInterface)
    }




}
