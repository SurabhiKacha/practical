package com.dvl.surabhipracticalnew.repository

import com.dvl.surabhipracticalnew.model.response.NewsListResponseBean
import com.dvl.surabhipracticalnew.retrofit.ApiInterface
import com.dvl.surabhipracticalnew.utils.Constant
import com.dvl.surabhipracticalnew.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException

class NewsListRepository
constructor(
    private val apiInterface: ApiInterface
) {


    /** news list api call */
    suspend fun newsLists(): Flow<DataState<NewsListResponseBean>> =
        flow {
            emit(DataState.Loading)
            try {
                val response = apiInterface.newsLists()
                if (response.status == Constant.API_RESPONSE_STATUS.SUCCESS) {
                    emit(DataState.Success(response))
                } else {
                    emit(DataState.CustomException("Exception"))
                }
            } catch (e: HttpException) {
                emit(DataState.Error(e))
            }
        }


}