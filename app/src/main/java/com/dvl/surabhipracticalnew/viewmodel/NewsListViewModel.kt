@file:Suppress("DEPRECATION")

package com.dvl.surabhipracticalnew.viewmodel

import androidx.lifecycle.*
import com.dvl.surabhipracticalnew.model.response.NewsListResponseBean
import com.dvl.surabhipracticalnew.repository.NewsListRepository
import com.dvl.surabhipracticalnew.utils.DataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
public class NewsListViewModel
@Inject
constructor(
    private val newsListRepository: NewsListRepository
) : ViewModel() {


    /** Sign In*/
    private val _newsListResponse: MutableLiveData<DataState<NewsListResponseBean>> =
        MutableLiveData()

    val newsListResponse: LiveData<DataState<NewsListResponseBean>>
        get() = _newsListResponse


    fun setStateEvent(newsListStateEvent: NewsListsStateEvent) {
        viewModelScope.launch {
            when (newsListStateEvent) {

                is NewsListsStateEvent.NewsList -> {
                    newsListRepository.newsLists()
                        .onEach {
                            _newsListResponse.value = it
                        }
                        .launchIn(viewModelScope)
                }

                else -> {

                }
            }
        }
    }
}

sealed class NewsListsStateEvent {
    object NewsList: NewsListsStateEvent()
}