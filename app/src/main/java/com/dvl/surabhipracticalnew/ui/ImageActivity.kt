package com.dvl.surabhipracticalnew.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.davemorrissey.labs.subscaleview.ImageSource
import com.dvl.surabhipracticalnew.R
import com.dvl.surabhipracticalnew.databinding.ActivityImageBinding
import com.dvl.surabhipracticalnew.databinding.ActivityNewsListDetailsBinding

class ImageActivity : AppCompatActivity() {
    private lateinit var binding: ActivityImageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityImageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var url = intent.getStringExtra("IMAGE")
        Log.e("TAG", "onCreate: "+url)
        binding.imageView.setImage(ImageSource.uri(url!!))
    }
}