package com.dvl.surabhipracticalnew.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dvl.surabhipracticalnew.R
import com.dvl.surabhipracticalnew.adapter.NewsListAdapter
import com.dvl.surabhipracticalnew.databinding.ActivityMainBinding
import com.dvl.surabhipracticalnew.model.response.NewsListResponseBean
import com.dvl.surabhipracticalnew.utils.Constant
import com.dvl.surabhipracticalnew.utils.DataState
import com.dvl.surabhipracticalnew.viewmodel.NewsListViewModel
import com.dvl.surabhipracticalnew.viewmodel.NewsListsStateEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity(),View.OnClickListener {
    private lateinit var binding: ActivityMainBinding

    private val mTAG = this::class.java.simpleName

    lateinit var newsListAdapter: NewsListAdapter
    lateinit var linearLayoutManager: LinearLayoutManager

    @ExperimentalCoroutinesApi
    private val viewModel: NewsListViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.setStateEvent(NewsListsStateEvent.NewsList)
        setObserver()

    }

    private fun setObserver() {
        viewModel.newsListResponse.observe(this, Observer { dataState ->
            when (dataState) {
                is DataState.Success<NewsListResponseBean> -> {
                    displayProgressBar(false)
                    if (dataState.data.status == Constant.API_RESPONSE_STATUS.SUCCESS) {


                        Log.d(mTAG, "setObserver: "+dataState.data.articles )
                        linearLayoutManager = LinearLayoutManager(this)
                        binding.recyclerView.layoutManager = linearLayoutManager
                        newsListAdapter =
                            NewsListAdapter(this, this, dataState.data.articles)
                        binding.recyclerView.adapter = newsListAdapter
                    }
                }
                is DataState.CustomException -> {
                    displayProgressBar(false)
                    displayError(dataState.message)
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.message}")
                }
                is DataState.Error -> {
                    displayProgressBar(false)
                    displayError(dataState.exception.message())
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception.message()}")
                    Log.d(mTAG, "ERROR MESSAGE-> ${dataState.exception}")
                }

                is DataState.Loading -> {
                    displayProgressBar(true)
                }
            }
        })
    }


    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this@MainActivity, "Unknown error", Toast.LENGTH_SHORT).show()
        }
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        binding.progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    override fun onClick(view: View?) {
        when(view!!.id)
        {
            R.id.constraintLayout->{
                var pos = view.tag.toString().toInt()
                var intent = Intent(this,NewsListDetailsActivity::class.java)
                intent.putExtra(Constant.NEWS_DETAILS,newsListAdapter.getList()[pos])
                startActivity(intent)
            }
        }
    }
}