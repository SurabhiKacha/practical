package com.dvl.surabhipracticalnew.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.dvl.surabhipracticalnew.R
import com.dvl.surabhipracticalnew.databinding.ActivityMainBinding
import com.dvl.surabhipracticalnew.databinding.ActivityNewsListDetailsBinding
import com.dvl.surabhipracticalnew.model.response.Article
import com.dvl.surabhipracticalnew.utils.Constant

class NewsListDetailsActivity : AppCompatActivity() {
    private val mTAG = this::class.java.simpleName

    private lateinit var binding: ActivityNewsListDetailsBinding
    lateinit var  articalList: Article
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewsListDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        articalList = (intent.extras!!.getSerializable(Constant.NEWS_DETAILS) as Article?)!!

        Log.d(mTAG, "onCreate: $articalList")

        initUi()
    }

    private fun initUi() {
        Glide.with(this)
            .load(articalList.urlToImage)
            .into(binding.imageNews)

        binding.txtNewsTitle.text = Html.fromHtml(
            "<p>${ articalList.title}</p>",
            Html.FROM_HTML_MODE_COMPACT)
        binding.txtDescription.text = Html.fromHtml(
            "<p>${ articalList.description}</p>",
            Html.FROM_HTML_MODE_COMPACT)
        binding.txtAuthorName.text = Html.fromHtml(
            "<p>${ articalList.author}</p>",
            Html.FROM_HTML_MODE_COMPACT)

        binding.txtDate.text = Html.fromHtml(
            "<p>${ articalList.publishedAt}</p>",
            Html.FROM_HTML_MODE_COMPACT)

        binding.txtWebLink.loadUrl(articalList.url)

        binding.imageNews.setOnClickListener(View.OnClickListener {
                var intent = Intent(this,ImageActivity::class.java)
            intent.putExtra("IMAGE",articalList.urlToImage)
            startActivity(intent)
        })
    }
}