package com.dvl.surabhipracticalnew.adapter

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.dvl.surabhipracticalnew.R
import com.dvl.surabhipracticalnew.model.response.Article
import com.dvl.surabhipracticalnew.utils.Utils

class NewsListAdapter(
    val context: Context,
    private val onClickListener: View.OnClickListener,
    private val list: List<Article>
) :
    RecyclerView.Adapter<NewsListAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_news, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.constraintLayout.tag = position
        holder.constraintLayout.setOnClickListener(onClickListener)
        holder.txtTitle.text = Html.fromHtml(
            "<p>${list[position].title}</p>",
            Html.FROM_HTML_MODE_COMPACT
        )
//        holder.txtDate.text =Utils.parseDateToddMMyyyy(list[position].publishedAt,)
        holder.txtDate.text =Html.fromHtml(
            "<p>${ list[position].publishedAt}</p>",
            Html.FROM_HTML_MODE_COMPACT)
        holder.txtAuthorName.text =
            Html.fromHtml(
                "<p>${list[position].author}</p>",
                Html.FROM_HTML_MODE_COMPACT
            )


    }


    fun getList(): List<Article> {
        return list
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val constraintLayout: ConstraintLayout = itemView.findViewById(R.id.constraintLayout)
        val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
        val txtAuthorName: TextView = itemView.findViewById(R.id.txtAuthorName)
        val txtDate: TextView = itemView.findViewById(R.id.txtDate)
    }
}