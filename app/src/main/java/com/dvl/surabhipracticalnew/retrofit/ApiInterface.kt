package com.dvl.surabhipracticalnew.retrofit

import com.dvl.surabhipracticalnew.model.response.NewsListResponseBean
import com.dvl.surabhipracticalnew.utils.ApiConstant
import retrofit2.http.*

interface ApiInterface {


    @GET(ApiConstant.NEWS_LIST)
    suspend fun newsLists(): NewsListResponseBean




}